/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.feedingthemachine.rest.examen.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author feedingthemachine
 */
@Entity
@Table(name = "sitios")
@NamedQueries({
    @NamedQuery(name = "Sitios.findAll", query = "SELECT s FROM Sitios s"),
    @NamedQuery(name = "Sitios.findById", query = "SELECT s FROM Sitios s WHERE s.id = :id"),
    @NamedQuery(name = "Sitios.findBySitio", query = "SELECT s FROM Sitios s WHERE s.sitio = :sitio"),
    @NamedQuery(name = "Sitios.findByComentario", query = "SELECT s FROM Sitios s WHERE s.comentario = :comentario"),
    @NamedQuery(name = "Sitios.findByWeb", query = "SELECT s FROM Sitios s WHERE s.web = :web"),
    @NamedQuery(name = "Sitios.findByPrioridad", query = "SELECT s FROM Sitios s WHERE s.prioridad = :prioridad")})
public class Sitios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "sitio")
    private String sitio;
    @Size(max = 2147483647)
    @Column(name = "comentario")
    private String comentario;
    @Size(max = 2147483647)
    @Column(name = "web")
    private String web;
    @Size(max = 2147483647)
    @Column(name = "prioridad")
    private String prioridad;

    public Sitios() {
    }

    public Sitios(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSitio() {
        return sitio;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sitios)) {
            return false;
        }
        Sitios other = (Sitios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.feedingthemachine.rest.examen.entity.Sitios[ id=" + id + " ]";
    }
    
}
