/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.feedingthemachine.rest.examen;

import com.feedingthemachine.rest.examen.dao.SitiosJpaController;
import com.feedingthemachine.rest.examen.dao.exceptions.NonexistentEntityException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.feedingthemachine.rest.examen.entity.Sitios;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
/**
 *
 * @author Felipe Veloso
 */
@Path("/sitios")
public class Sites {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSitios(){
        
        SitiosJpaController dao = new SitiosJpaController();
        List<Sitios> listado = dao.findSitiosEntities();
        
        return Response.ok(200).entity(listado).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response guardarSitio(Sitios sitio){
        
        SitiosJpaController dao = new SitiosJpaController();
        try {
            dao.create(sitio);
        } catch (Exception ex) {
            Logger.getLogger(Sites.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(sitio).build();
    }
    
    @DELETE
    @Path("/{iddestroy}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSitio(@PathParam("iddestroy") int iddelete){
        
        SitiosJpaController dao = new SitiosJpaController();
        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Sites.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("Sitio eliminado").build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateSitio(Sitios sitio){
        
        SitiosJpaController dao = new SitiosJpaController();
        try {
            dao.edit(sitio);
        } catch (Exception ex) {
            Logger.getLogger(Sites.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(sitio).build();
    }
}
