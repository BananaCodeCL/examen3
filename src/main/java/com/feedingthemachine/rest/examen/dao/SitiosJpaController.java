/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.feedingthemachine.rest.examen.dao;

import com.feedingthemachine.rest.examen.dao.exceptions.NonexistentEntityException;
import com.feedingthemachine.rest.examen.dao.exceptions.PreexistingEntityException;
import com.feedingthemachine.rest.examen.entity.Sitios;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author feedingthemachine
 */
public class SitiosJpaController implements Serializable {

    public SitiosJpaController() {
    }
    
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sitios sitios) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(sitios);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSitios(sitios.getId()) != null) {
                throw new PreexistingEntityException("Sitios " + sitios + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sitios sitios) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            sitios = em.merge(sitios);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sitios.getId();
                if (findSitios(id) == null) {
                    throw new NonexistentEntityException("The sitios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sitios sitios;
            try {
                sitios = em.getReference(Sitios.class, id);
                sitios.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sitios with id " + id + " no longer exists.", enfe);
            }
            em.remove(sitios);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sitios> findSitiosEntities() {
        return findSitiosEntities(true, -1, -1);
    }

    public List<Sitios> findSitiosEntities(int maxResults, int firstResult) {
        return findSitiosEntities(false, maxResults, firstResult);
    }

    private List<Sitios> findSitiosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sitios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sitios findSitios(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sitios.class, id);
        } finally {
            em.close();
        }
    }

    public int getSitiosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sitios> rt = cq.from(Sitios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
