<%-- 
    Document   : index
    Created on : 3 may. 2021, 16:28:57
    Author     : feedingthemachine
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Felipe Veloso - Seccion 50 </h1>
        <p>Get: http://localhost:8080/rest-examen-1.0-SNAPSHOT/api/sitios</p>
        <p>Post: http://localhost:8080/rest-examen-1.0-SNAPSHOT/api/sitios</p>
        <p>Edit: http://localhost:8080/rest-examen-1.0-SNAPSHOT/api/sitios</p>
        <p>Delete: http://localhost:8080/rest-examen-1.0-SNAPSHOT/api/sitios/{iddestroy}</p>
        <h2>
            {
            "id": int,
            "sitio": "string",
            "comentario": "string"
            "web": "string"
            "prioridad": "string"
            }
        </h2>
    </body>
</html>
